package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TipoVehiculo")
public class TipoVehiculo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*Atributos*/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_vehiculo_pk", nullable=false)
	private long id;
	
	@Column(name="Descripción", nullable=false)
	private String Descripcion;

	/*Constructores*/
	
	public TipoVehiculo(long id, String descripcion) {
		super();
		this.id = id;
		Descripcion = descripcion;
	}

	public TipoVehiculo() {
		super();
	}
	
	/*Metodos*/

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "TipoVehiculo [id=" + id + ", Descripcion=" + Descripcion + "]";
	}
	
	
	
	

}
