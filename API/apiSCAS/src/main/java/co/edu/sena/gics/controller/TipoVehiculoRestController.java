package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.TipoVehiculo;
import co.edu.sena.gics.servicesInterface.ITipoVehiculoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})

public class TipoVehiculoRestController {
	
	@Autowired
	private ITipoVehiculoServices tipoVehiculoService;
	
	@GetMapping("/tipoVehiculo")
	public List<TipoVehiculo> getAll(){
		return tipoVehiculoService.findAll();
	}
	
	@PostMapping("/tipoVehiculo")
	public TipoVehiculo create(@RequestBody TipoVehiculo tipoVehiculo) {
		System.out.println(tipoVehiculo.toString());
		return tipoVehiculoService.create(tipoVehiculo);
	}


}
