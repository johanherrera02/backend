package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="espacio_parqueadero")
public class EspacioParqueadero implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_espacio_parqueadero_pk", nullable=false)
	private long id;
	
	@Column(name="nuemero", nullable=false)
	private int nuemero;

	/*Constructores*/
	public EspacioParqueadero(long id, int nuemero) {
		super();
		this.id = id;
		this.nuemero = nuemero;
	}
	

	public EspacioParqueadero() {
		super();
	}

	/* metodos*/
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNuemero() {
		return nuemero;
	}

	public void setNuemero(int nuemero) {
		this.nuemero = nuemero;
	}


	@Override
	public String toString() {
		return "EspacioParqueadero [id=" + id + ", nuemero=" + nuemero + "]";
	}

	
	
	
}
