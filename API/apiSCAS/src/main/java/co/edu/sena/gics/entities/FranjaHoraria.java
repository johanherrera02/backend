package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="franja_horaria")
public class FranjaHoraria implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_franja_horaria_pk", nullable=false)
	private long id;
	
	@Column(name="descripcion", nullable=false)
	private String descripcion;

	/* constructores*/
	
	public FranjaHoraria(long id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public FranjaHoraria() {
		super();
	}
	
	/* metodos*/

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "FranjaHoraria [id=" + id + ", descripcion=" + descripcion + "]";
	}
	
	
}
