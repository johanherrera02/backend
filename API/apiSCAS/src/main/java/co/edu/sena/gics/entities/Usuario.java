package co.edu.sena.gics.entities;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries(value = { 
		@NamedQuery (name="Usuario.findByIdDocumento", query="SELECT us FROM Usuario us WHERE us.tipo_documento.id = :idDocumento"),
		@NamedQuery(name="Usuario.findByPassUsuario", query="SELECT us FROM Usuario us WHERE us.password = :pass AND us.numero = :numero")
		})

@Entity
@Table(name="usuario")

public class Usuario implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario_pk", nullable=false)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "tipo_documento", nullable=false)
	private Documento tipo_documento;

	
	@Column(name="numero_documento", nullable=false)
	private long numero;
	
	@Column(name="nombres", nullable=false)
	private String nombres;
	
	@Column(name="apellidos", nullable=false)
	private String apellidos;
	
	@Column(name="telefono", nullable=false)
	private long telefono;
	
	@Column(name="email", nullable=false)
	private String email;
	
	@Column(name="password", nullable=false)
	private String password;
	
	/*Constructores*/

	public Usuario() {
		super();
	}

	public Usuario(long id, Documento tipo_documento, long numero, String nombres, String apellidos, long telefono,
			String email, String password) {
		super();
		this.id = id;
		this.tipo_documento = tipo_documento;
		this.numero = numero;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.telefono = telefono;
		this.email = email;
		this.password = password;
	}

	/*Metodos*/
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Documento getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(Documento tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public long getTelefono() {
		return telefono;
	}

	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", tipo_documento=" + tipo_documento + ", numero=" + numero + ", nombres="
				+ nombres + ", apellidos=" + apellidos + ", telefono=" + telefono + ", email=" + email + ", password="
				+ password + "]";
	}
	
	
	

	
	
	
}
