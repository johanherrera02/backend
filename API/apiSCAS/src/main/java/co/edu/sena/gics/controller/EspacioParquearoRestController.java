package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.EspacioParqueadero;
import co.edu.sena.gics.servicesInterface.IEspacioParqueaderoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})

public class EspacioParquearoRestController {

	@Autowired
	private IEspacioParqueaderoServices espacioParquearoServices;
	
	@GetMapping("/espacioParqueadero")
	public List<EspacioParqueadero> getAll(){
		return espacioParquearoServices.findAll();
	}
	
	@PostMapping("/espacioParqueadero")
	public EspacioParqueadero create(@RequestBody EspacioParqueadero espacioParquearo) {
		System.out.println(espacioParquearo.toString());
		return espacioParquearoServices.create(espacioParquearo);
	}

}
