package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.Vehiculo;

public interface IVehiculoServices {
public List<Vehiculo> findAll();
	
	public Vehiculo create(Vehiculo vehiculo);

	public Vehiculo update(long id, Vehiculo vehiculo);

	public void delete(long id); 
}
