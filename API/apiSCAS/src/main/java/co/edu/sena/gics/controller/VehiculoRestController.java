package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.Vehiculo;
import co.edu.sena.gics.servicesInterface.IVehiculoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})

public class VehiculoRestController {
	
	@Autowired
	private IVehiculoServices vehiculoService;

	@GetMapping("/vehiculo")
	public List<Vehiculo> getAll(){
		return vehiculoService.findAll();
	}

	@PostMapping("/vehiculo")
	public Vehiculo create(@RequestBody Vehiculo vehiculo) {
		System.out.println(vehiculo.toString());
		return vehiculoService.create(vehiculo);
	}

	@PutMapping("/vehiculo/{id}")
	public Vehiculo update(@PathVariable long id, @RequestBody Vehiculo vehiculo){
		System.out.println(id);
		System.out.println(vehiculo);
		return this.vehiculoService.update(id, vehiculo);		
	}
	
	@DeleteMapping("/vehiculo/{id}")
	public void delete(@PathVariable long id) {
		this.vehiculoService.delete(id);
	}


}
