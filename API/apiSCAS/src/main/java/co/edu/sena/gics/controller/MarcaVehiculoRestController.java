package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.MarcaVehiculo;
import co.edu.sena.gics.servicesInterface.IMarcaVehiculoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})

public class MarcaVehiculoRestController {

	@Autowired
	private IMarcaVehiculoServices marcaVehiculoServices ;
	
	@GetMapping("/marcaVehiculo")
	public List<MarcaVehiculo> getAll(){
		return marcaVehiculoServices.findAll();
	}
	
	@PostMapping("/marcaVehiculo")
	public MarcaVehiculo create(@RequestBody MarcaVehiculo marcaVehiculo) {
		return marcaVehiculoServices.create(marcaVehiculo);
	}


}
