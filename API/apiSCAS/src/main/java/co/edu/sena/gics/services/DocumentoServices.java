package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IDocumentoDao;
import co.edu.sena.gics.entities.Documento;
import co.edu.sena.gics.servicesInterface.IDocumentoServices;

@Service
public class DocumentoServices implements IDocumentoServices{

	@Autowired
	private IDocumentoDao documentoDao;
	
	@Override
	public List<Documento> findALL() {
		return (List<Documento>) documentoDao.findAll();
	}

	@Override
	public Documento create(Documento documento) {
		return documentoDao.save(documento);
	}

}
