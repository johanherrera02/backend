package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.FranjaHoraria;

public interface IFranjaHorariaServices {

	List<FranjaHoraria> findAll();

	FranjaHoraria create(FranjaHoraria franjaHoraria);

}
