package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IFranjaHorariaDao;
import co.edu.sena.gics.entities.FranjaHoraria;
import co.edu.sena.gics.servicesInterface.IFranjaHorariaServices;

@Service
public class FranjaHorariaServices implements IFranjaHorariaServices{
	
	@Autowired
	private IFranjaHorariaDao franjaHorariaDao;

	@Override
	public List<FranjaHoraria> findAll() {
		return (List<FranjaHoraria>) franjaHorariaDao.findAll();
	}

	@Override
	public FranjaHoraria create(FranjaHoraria franjaHoraria) {
		return (FranjaHoraria) franjaHorariaDao.save(franjaHoraria);
	}


}
