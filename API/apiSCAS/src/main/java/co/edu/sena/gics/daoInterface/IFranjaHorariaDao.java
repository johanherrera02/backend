package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.FranjaHoraria;

public interface IFranjaHorariaDao extends CrudRepository<FranjaHoraria, Long>{

}
