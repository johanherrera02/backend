package co.edu.sena.gics.daoInterface;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{
	
	public List<Usuario> findByIdDocumento(long idDocumento);

	public Usuario findByPassUsuario(String pass, String numero);
}
