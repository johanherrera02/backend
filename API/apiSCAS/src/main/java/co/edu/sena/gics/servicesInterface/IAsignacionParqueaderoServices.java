package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.AsignacionParqueadero;

public interface IAsignacionParqueaderoServices {

	public List<AsignacionParqueadero> findAll();

	public AsignacionParqueadero create(AsignacionParqueadero asignacionParqueadero);

	public AsignacionParqueadero update(long id, AsignacionParqueadero asignacionParqueadero);

	public void delete(long id);

}
