package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.Documento;

public interface IDocumentoDao extends CrudRepository<Documento, Long>{

}
