package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.FranjaHoraria;
import co.edu.sena.gics.servicesInterface.IFranjaHorariaServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})

public class FranjaHorariaRestController {

	@Autowired
	private IFranjaHorariaServices franjaHorariaServices;
	
	@GetMapping("/franjaHoraria")
	public List<FranjaHoraria> getAll(){
		return franjaHorariaServices.findAll();
	}

	@PostMapping("/franjaHoraria")
	public FranjaHoraria create(@RequestBody FranjaHoraria franjaHoraria) {
		System.out.println(franjaHoraria.toString());
		return franjaHorariaServices.create(franjaHoraria);
	}

}
