package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import co.edu.sena.gics.entities.Usuario;
import co.edu.sena.gics.servicesInterface.IUsuarioServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })

public class UsuarioRestController {

	@Autowired
	private IUsuarioServices usuarioService;
	
	@GetMapping("/usuario")
	public List<Usuario> getAll(){
		return usuarioService.findALL();
	}
	
	@PostMapping("/usuario")
	public Usuario create(@RequestBody Usuario usuario) {
		System.out.println(usuario.toString());
		return usuarioService.create(usuario);
	}
	
	@PutMapping("/usuario/{id}")
	public Usuario update(@PathVariable int id, @RequestBody Usuario usuario){
		System.out.println(id);
		System.out.println(usuario);
		return this.usuarioService.update(id, usuario);		
	}
	
	@DeleteMapping("/usuario/{id}")
	public void delete(@PathVariable int id) {
		this.usuarioService.delete(id);
	}

	@GetMapping("/usuario/documento/{id}")
	public List<Usuario> findUserById(@PathVariable int id){
		return this.usuarioService.findUsuarioById(id);
	}


}
