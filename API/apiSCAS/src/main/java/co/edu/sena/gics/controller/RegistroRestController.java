package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.Registro;
import co.edu.sena.gics.servicesInterface.IRegistroServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})

public class RegistroRestController {

	@Autowired
	private IRegistroServices registroServices;
	
	@GetMapping("/registro")
	public List<Registro> getAll(){
		return registroServices.findAll();
	}

	@PostMapping("/registro")
	public Registro create(@RequestBody Registro registro) {
		System.out.println(registro.toString());
		return registroServices.create(registro);
	}

}
