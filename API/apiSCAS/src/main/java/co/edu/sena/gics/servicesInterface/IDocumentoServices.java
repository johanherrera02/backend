package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.Documento;

public interface IDocumentoServices {

	public List<Documento> findALL();
	
	public Documento create(Documento documento); 
} 
