package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.AsignacionParqueadero;

public interface IAsignacionParqueaderoDao extends CrudRepository<AsignacionParqueadero, Long>{

}
