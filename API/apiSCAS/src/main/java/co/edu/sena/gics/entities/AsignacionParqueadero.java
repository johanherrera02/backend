package co.edu.sena.gics.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="asignacion_parqueadero")

public class AsignacionParqueadero implements Serializable{

	private static final long serialVersionUID = 1L;

	/* Atributos */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignacion_parqueaderor_pk", nullable=false)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "id_vehiculo", nullable=false)
	private Vehiculo vehiculo;

	@Column(name="fecha_ingreso", nullable=false)
	private Date fechaIngreso;
	
	@Column(name="email", nullable=false)
	private String email;

	@OneToOne
	@JoinColumn(name = "id_espacio_parqueadero", nullable=false)
	private EspacioParqueadero espacioParqueadero;
	
	@OneToOne
	@JoinColumn(name = "id_franja_horaria", nullable=false)
	private FranjaHoraria franjaHoraria;

	public AsignacionParqueadero() {
		super();
	}

	public AsignacionParqueadero(long id, Vehiculo vehiculo, Date fechaIngreso, String email,
			EspacioParqueadero espacioParqueadero, FranjaHoraria franjaHoraria) {
		super();
		this.id = id;
		this.vehiculo = vehiculo;
		this.fechaIngreso = fechaIngreso;
		this.email = email;
		this.espacioParqueadero = espacioParqueadero;
		this.franjaHoraria = franjaHoraria;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EspacioParqueadero getEspacioParqueadero() {
		return espacioParqueadero;
	}

	public void setEspacioParqueadero(EspacioParqueadero espacioParqueadero) {
		this.espacioParqueadero = espacioParqueadero;
	}

	public FranjaHoraria getFranjaHoraria() {
		return franjaHoraria;
	}

	public void setFranjaHoraria(FranjaHoraria franjaHoraria) {
		this.franjaHoraria = franjaHoraria;
	}

	@Override
	public String toString() {
		return "AsignacionParqueadero [id=" + id + ", vehiculo=" + vehiculo + ", fechaIngreso=" + fechaIngreso
				+ ", email=" + email + ", espacioParqueadero=" + espacioParqueadero + ", franjaHoraria=" + franjaHoraria
				+ "]";
	}

	
	
}
