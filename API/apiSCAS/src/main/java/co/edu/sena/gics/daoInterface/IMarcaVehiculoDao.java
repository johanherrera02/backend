package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.MarcaVehiculo;

public interface IMarcaVehiculoDao extends CrudRepository<MarcaVehiculo, Long>{

}
