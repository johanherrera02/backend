package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.AsignacionParqueadero;
import co.edu.sena.gics.servicesInterface.IAsignacionParqueaderoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})

public class AsignacionParqueaderoRestController {

	@Autowired
	private IAsignacionParqueaderoServices asignacionParqueaderoServices;
	
	@GetMapping("/asignacionParqueadero")
	public List<AsignacionParqueadero> getAll(){
		return asignacionParqueaderoServices.findAll();
	}
	
	@PostMapping("/asignacionParqueadero")
	public AsignacionParqueadero create(@RequestBody AsignacionParqueadero asignacionParqueadero) {
		System.out.println(asignacionParqueadero.toString());
		return asignacionParqueaderoServices.create(asignacionParqueadero);
	}

	@PutMapping("/asignacionParqueadero/{id}")
	public AsignacionParqueadero update(@PathVariable int id, @RequestBody AsignacionParqueadero asignacionParqueadero){
		System.out.println(id);
		System.out.println(asignacionParqueadero);
		return this.asignacionParqueaderoServices.update(id, asignacionParqueadero);		
	}

	@DeleteMapping("/asignacionParqueadero/{id}")
	public void delete(@PathVariable int id) {
		this.asignacionParqueaderoServices.delete(id);
	}
	

}
