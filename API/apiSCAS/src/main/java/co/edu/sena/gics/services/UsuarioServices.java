package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import co.edu.sena.gics.daoInterface.IUsuarioDao;
import co.edu.sena.gics.entities.Usuario;
import co.edu.sena.gics.servicesInterface.IUsuarioServices;

@Service
public class UsuarioServices implements IUsuarioServices{

	@Autowired
	private IUsuarioDao usuarioDao;
	
	@Override
	public List<Usuario> findALL() {
		return (List<Usuario>) usuarioDao.findAll();
	}

	@Override
	public Usuario create(Usuario usuario) {
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuario update(long id, Usuario usuario) {
		Optional<Usuario> us = this.usuarioDao.findById(id);
		if(us.isPresent()) {
			usuario.setId(us.get().getId());
			return this.usuarioDao.save(usuario);
		}
		else {
			return new Usuario();
		}

	}
	
	@Override
	public void delete(long id) {
		this.usuarioDao.deleteById(id);
	}

	@Override
	public List<Usuario> findUsuarioById(int id) {
		return this.usuarioDao.findByIdDocumento(id);
	}

}
