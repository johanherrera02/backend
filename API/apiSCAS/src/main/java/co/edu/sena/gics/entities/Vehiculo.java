package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Vehiculo")
public class Vehiculo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*Atributos*/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_Vehiculo__pk", nullable=false)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable=false)
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name = "id_tipo_vehiculo", nullable=false)
	private TipoVehiculo tipoVehiculo;

	@ManyToOne
	@JoinColumn(name = "id_marca_vehiculo", nullable=false)
	private MarcaVehiculo marcaVehiculo;

	@Column(name="color", nullable=false)
	private String color;

	@Column(name="placa", nullable=false)
	private String placa;

	@Column(name="soat", nullable=false)
	private String soat;

	@Column(name="tecnomecanica", nullable=false)
	private String tecnomecanica;

	@Column(name="foto", nullable=false)
	private String foto;

	public Vehiculo() {
		super();
	}

	public Vehiculo(long id, Usuario usuario, TipoVehiculo tipoVehiculo, MarcaVehiculo marcaVehiculo, String color,
			String placa, String soat, String tecnomecanica, String foto) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.tipoVehiculo = tipoVehiculo;
		this.marcaVehiculo = marcaVehiculo;
		this.color = color;
		this.placa = placa;
		this.soat = soat;
		this.tecnomecanica = tecnomecanica;
		this.foto = foto;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoVehiculo getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public MarcaVehiculo getMarcaVehiculo() {
		return marcaVehiculo;
	}

	public void setMarcaVehiculo(MarcaVehiculo marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getSoat() {
		return soat;
	}

	public void setSoat(String soat) {
		this.soat = soat;
	}

	public String getTecnomecanica() {
		return tecnomecanica;
	}

	public void setTecnomecanica(String tecnomecanica) {
		this.tecnomecanica = tecnomecanica;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	@Override
	public String toString() {
		return "Vehiculo [id=" + id + ", usuario=" + usuario + ", tipoVehiculo=" + tipoVehiculo + ", marcaVehiculo="
				+ marcaVehiculo + ", color=" + color + ", placa=" + placa + ", soat=" + soat + ", tecnomecanica="
				+ tecnomecanica + ", foto=" + foto + "]";
	}

	
}
