package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipo_documento")
public class Documento implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_documento_pk", nullable=false)
	private long id;
	
	@Column(name="Tipo", nullable=false)
	private String tipo;
	
	/* constructores*/

	public Documento() {
		super();
	}

	public Documento(long id, String tipo) {
		super();
		this.id = id;
		this.tipo = tipo;
	}

	
	/* metodos*/
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Documento [id=" + id + ", tipo=" + tipo + "]";
	}

	
}
