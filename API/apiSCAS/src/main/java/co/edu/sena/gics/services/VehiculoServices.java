package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IVehiculoDao;
import co.edu.sena.gics.entities.Vehiculo;
import co.edu.sena.gics.servicesInterface.IVehiculoServices;

@Service
public class VehiculoServices implements IVehiculoServices{

	@Autowired
	private IVehiculoDao vehiculoDao;
	
	@Override
	public List<Vehiculo> findAll() {
		return (List<Vehiculo>) vehiculoDao.findAll();
	}

	@Override
	public Vehiculo create(Vehiculo vehiculo) {
		return (Vehiculo) vehiculoDao.save(vehiculo);
	}

	@Override
	public Vehiculo update(long id, Vehiculo vehiculo) {
		Optional<Vehiculo> vh = this.vehiculoDao.findById(id);
		if(vh.isPresent()) {
			vehiculo.setId(vh.get().getId());
			return this.vehiculoDao.save(vehiculo);
		}
		else {
			return new Vehiculo();
		}
	}


	@Override
	public void delete(long id) {
		this.vehiculoDao.deleteById(id);
	}

}
