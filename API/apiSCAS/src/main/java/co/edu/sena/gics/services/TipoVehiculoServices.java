package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.ITipoVehiculoDao;
import co.edu.sena.gics.entities.TipoVehiculo;
import co.edu.sena.gics.servicesInterface.ITipoVehiculoServices;

@Service
public class TipoVehiculoServices implements ITipoVehiculoServices{

	@Autowired
	private ITipoVehiculoDao tipoVehiculoDao;

	@Override
	public List<TipoVehiculo> findAll() {
		return (List<TipoVehiculo>) tipoVehiculoDao.findAll();
	}

	@Override
	public TipoVehiculo create(TipoVehiculo tipoVehiculo) {
		return (TipoVehiculo) tipoVehiculoDao.save(tipoVehiculo);
	}

}
