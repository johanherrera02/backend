package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.TipoVehiculo;

public interface ITipoVehiculoDao extends CrudRepository<TipoVehiculo, Long>{

}
