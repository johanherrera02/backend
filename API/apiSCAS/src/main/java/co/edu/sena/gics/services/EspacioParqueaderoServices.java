package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IEspacioParqueaderoDao;
import co.edu.sena.gics.entities.EspacioParqueadero;
import co.edu.sena.gics.servicesInterface.IEspacioParqueaderoServices;

@Service
public class EspacioParqueaderoServices implements IEspacioParqueaderoServices{
	
	@Autowired
	private IEspacioParqueaderoDao espacioParqueaderoDao;
	
	@Override
	public List<EspacioParqueadero> findAll() {
		return (List<EspacioParqueadero>) espacioParqueaderoDao.findAll();
	}

	@Override
	public EspacioParqueadero create(EspacioParqueadero espacioParqueadero) {
		return (EspacioParqueadero) espacioParqueaderoDao.save(espacioParqueadero);
	}
	

}
