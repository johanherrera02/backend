package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.Registro;

public interface IRegistroDao extends CrudRepository<Registro, Long>{

}
