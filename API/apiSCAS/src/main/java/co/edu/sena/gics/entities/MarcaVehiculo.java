package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MarcaVehiculo")
public class MarcaVehiculo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*Atributos*/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_marca_vehiculo_pk", nullable=false)
	private long id;
	
	@Column(name="marca", nullable=false)
	private String marca;
	
	/*Constructores*/
	public MarcaVehiculo(long id, String marca) {
		super();
		this.id = id;
		this.marca = marca;
	}
	
	/*Metodos*/
	
	public MarcaVehiculo() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public String toString() {
		return "MarcaVehiculo [id=" + id + ", marca=" + marca + "]";
	}
	
	
	
}
