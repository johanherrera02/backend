package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.Usuario;


public interface IUsuarioServices {

	public List<Usuario> findALL();
	
	public Usuario create(Usuario usuario);

	public Usuario update(long id, Usuario usuario);

	public void delete(long id);

	public List<Usuario> findUsuarioById(int id); 

}
