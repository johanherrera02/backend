package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.EspacioParqueadero;

public interface IEspacioParqueaderoDao extends CrudRepository<EspacioParqueadero, Long>{

}
