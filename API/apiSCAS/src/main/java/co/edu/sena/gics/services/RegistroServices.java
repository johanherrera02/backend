package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IRegistroDao;
import co.edu.sena.gics.entities.Registro;
import co.edu.sena.gics.servicesInterface.IRegistroServices;

@Service
public class RegistroServices implements IRegistroServices{
	
	@Autowired
	private IRegistroDao registroDao;
	
	@Override
	public List<Registro> findAll() {
		return (List<Registro>) registroDao.findAll();
	}

	@Override
	public Registro create(Registro registro) {
		return (Registro) registroDao.save(registro);
	}


}
