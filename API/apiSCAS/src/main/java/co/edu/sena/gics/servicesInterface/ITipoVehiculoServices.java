package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.TipoVehiculo;

public interface ITipoVehiculoServices {
	
	public List<TipoVehiculo> findAll();
	
	public TipoVehiculo create(TipoVehiculo tipoVehiculo); 
}
