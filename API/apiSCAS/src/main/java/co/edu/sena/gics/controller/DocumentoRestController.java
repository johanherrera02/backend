package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.Documento;
import co.edu.sena.gics.servicesInterface.IDocumentoServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST })

public class DocumentoRestController {

	@Autowired
	private IDocumentoServices documentoService;
	
	@GetMapping("/documento")
	public List<Documento> getAll(){
		return documentoService.findALL();	
	}
	
	@PostMapping("/documento")
	public Documento create(@RequestBody Documento documento) {
		System.out.println(documento.toString());
		return documentoService.create(documento);
	}

}
