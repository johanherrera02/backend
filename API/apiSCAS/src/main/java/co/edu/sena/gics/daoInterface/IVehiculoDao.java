package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.Vehiculo;

public interface IVehiculoDao extends CrudRepository<Vehiculo, Long>{

}
