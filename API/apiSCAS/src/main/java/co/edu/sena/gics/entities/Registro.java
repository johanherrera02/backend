package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="registro")
public class Registro implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_registro_pk", nullable=false)
	private long id;

	@ManyToOne
	@JoinColumn(name = "id_asignacion_parqueaderor", nullable=false)
	private AsignacionParqueadero asignacionParqueadero;

	public Registro(long id, AsignacionParqueadero asignacionParqueadero) {
		super();
		this.id = id;
		this.asignacionParqueadero = asignacionParqueadero;
	}

	public Registro() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public AsignacionParqueadero getAsignacionParqueadero() {
		return asignacionParqueadero;
	}

	public void setAsignacionParqueadero(AsignacionParqueadero asignacionParqueadero) {
		this.asignacionParqueadero = asignacionParqueadero;
	}

	@Override
	public String toString() {
		return "Registro [id=" + id + ", asignacionParqueadero=" + asignacionParqueadero + "]";
	}

	
	
	
}
