package co.edu.sena.gics.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IAsignacionParqueaderoDao;
import co.edu.sena.gics.entities.AsignacionParqueadero;
import co.edu.sena.gics.servicesInterface.IAsignacionParqueaderoServices;

@Service
public class AsignacionParqueaderoServices implements IAsignacionParqueaderoServices{

	@Autowired
	private IAsignacionParqueaderoDao asignacionParqueaderoDao;

	@Override
	public List<AsignacionParqueadero> findAll() {
		return (List<AsignacionParqueadero>) asignacionParqueaderoDao.findAll();
	}
	
	@Override
	public AsignacionParqueadero create(AsignacionParqueadero asignacionParqueadero) {
		return asignacionParqueaderoDao.save(asignacionParqueadero);
	}

	@Override
	public AsignacionParqueadero update(long id, AsignacionParqueadero asignacionParqueadero) {
		Optional<AsignacionParqueadero> ap = this.asignacionParqueaderoDao.findById(id);
		if(ap.isPresent()) {
			asignacionParqueadero.setId(ap.get().getId());
			return this.asignacionParqueaderoDao.save(asignacionParqueadero);
		}
		else {
			return new AsignacionParqueadero();
		}
	}
	
	@Override
	public void delete(long id) {
		this.asignacionParqueaderoDao.deleteById(id);
	}

}
