package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.Registro;

public interface IRegistroServices {

	List<Registro> findAll();

	Registro create(Registro registro);

}
