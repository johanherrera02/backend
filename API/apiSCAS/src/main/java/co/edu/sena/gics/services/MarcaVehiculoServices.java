package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IMarcaVehiculoDao;
import co.edu.sena.gics.entities.MarcaVehiculo;
import co.edu.sena.gics.servicesInterface.IMarcaVehiculoServices;

@Service
public class MarcaVehiculoServices implements IMarcaVehiculoServices {

	@Autowired
	private IMarcaVehiculoDao marcaVehiculoDao;

	@Override
	public List<MarcaVehiculo> findAll() {
		return (List<MarcaVehiculo>) marcaVehiculoDao.findAll();
	}

	@Override
	public MarcaVehiculo create(MarcaVehiculo marcaVehiculo) {
		return (MarcaVehiculo) marcaVehiculoDao.save(marcaVehiculo);
	}

}
