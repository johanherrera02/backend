package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.EspacioParqueadero;

public interface IEspacioParqueaderoServices {

	List<EspacioParqueadero> findAll();

	EspacioParqueadero create(EspacioParqueadero espacioParqueadero);

}
