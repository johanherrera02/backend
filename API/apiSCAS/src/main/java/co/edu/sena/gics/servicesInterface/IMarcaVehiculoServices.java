package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.MarcaVehiculo;

public interface IMarcaVehiculoServices {

	public List<MarcaVehiculo> findAll();
		
		public MarcaVehiculo create(MarcaVehiculo marcaVehiculo); 

}
