package co.edu.sena.gics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiScasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiScasApplication.class, args);
	}

}
